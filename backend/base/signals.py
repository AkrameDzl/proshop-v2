from django.db.models.signals import pre_save
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from .models import Profile,UserShippingAddress
from django.dispatch import receiver


def updateUser(sender,instance,**kwargs):
    user = instance
    if user.email  !='':
        user.username = user.email

pre_save.connect(updateUser, sender=User)  


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


@receiver(post_save, sender=User)
def create_user_shipping(sender, instance, created, **kwargs):
    if created:
        UserShippingAddress.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_shipping(sender, instance, **kwargs):
    instance.usershippingaddress.save()

