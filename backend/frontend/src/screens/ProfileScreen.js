import React, { useState, useEffect } from "react"
import axios from "axios"
import { Link } from "react-router-dom"
import {
  Form,
  Button,
  Row,
  Col,
  Table,
  Image,
  ListGroup,
} from "react-bootstrap"
import { LinkContainer } from "react-router-bootstrap"
import { useDispatch, useSelector } from "react-redux"
import Loader from "../components/Loader"
import Message from "../components/Message"
import { getUserDetails, updateUserProfile } from "../actions/userActions"
import { USER_UPDATE_PROFILE_RESET } from "../constants/userConstant"
import { listMyOrders } from "../actions/orderActions"

function ProfileScreen({ history }) {
  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")
  const [message, setMessage] = useState("")
  const [uploading, setUploading] = useState(false)
  const [image, setImage] = useState("")
  const [address, setAddress] = useState(" ")
  const [city, setCity] = useState(" ")
  const [postalCode, setPostalCode] = useState(" ")
  const [country, setCountry] = useState(" ")
  const [clickedImg, setClickedImg] = useState(false)
  const [clickedInfo, setClickedInfo] = useState(false)
  const [clickedShip, setClickedShip] = useState(false)

  const dispatch = useDispatch()
  const userDetails = useSelector((state) => state.userDetails)
  const { error, loading, user } = userDetails

  const userLogin = useSelector((state) => state.userLogin)
  const { userInfo } = userLogin

  const userUpdateProfile = useSelector((state) => state.userUpdateProfile)
  const { success } = userUpdateProfile

  const orderListMy = useSelector((state) => state.orderListMy)
  const { loading: loadingOrders, error: errorOrders, orders } = orderListMy

  useEffect(() => {
    if (!userInfo) {
      history.push("/login")
    } else {
      if (!user || !user.name || success || userInfo._id !== user._id) {
        dispatch({ type: USER_UPDATE_PROFILE_RESET })
        dispatch(getUserDetails("profile"))
        dispatch(listMyOrders())
      } else {
        setName(user.name)
        setEmail(user.email)
        setImage(user.image)
        setAddress(user.address)
        setCity(user.city)
        setPostalCode(user.postalCode)
        setCountry(user.country)
        dispatch(listMyOrders())
      }
    }
  }, [dispatch, history, userInfo, user, success])
  const submitHandler = (e) => {
    e.preventDefault()
    if (password !== confirmPassword) {
      setMessage("passwords do not match")
    } else {
      dispatch(
        updateUserProfile({
          id: user._id,
          name: name,
          image,
          email: email,
          password: password,
          address: address,
          city: city,
          postalCode: postalCode,
          country: country,
        })
      )
      if (clickedShip == true) {
        setClickedShip(!clickedShip)
      }
      if (clickedInfo == true) {
        setClickedInfo(!clickedInfo)
      }
      setMessage("")
    }
  }
  const uploadFileHandler = async (e) => {
    const file = e.target.files[0]
    const formData = new FormData()
    formData.append("user_id", user._id)
    formData.append("image", file)
    setUploading(true)
    try {
      const config = {
        headers: { "Content-Type": "multipart/form-data" },
      }
      const { data } = await axios.post(
        "/api/users/profile/upload/",
        formData,
        config
      )
      setImage(data)
      setUploading(false)
      setClickedImg(!clickedImg)
      dispatch(getUserDetails("profile"))
    } catch (error) {
      setUploading(false)
    }
  }

  return (
    <div>
      <Row>
        <h2>UserProfile</h2>
        {message && <Message variant='danger'>{message}</Message>}
        {error && <Message variant='danger'>{error}</Message>}
        {loading && <Loader />}
        <Col md={4}>
          <Form.Group controlId='image' className='py-2'>
            <Form.Label>Profile Picture</Form.Label>{" "}
            <Button
              onClick={() => setClickedImg(!clickedImg)}
              variant='light'
              className='btn-sm'>
              <i className='fas fa-edit'></i>
            </Button>
            <Col xs={6} md={10}>
              <Image src={user.image} fluid rounded />
            </Col>
            {clickedImg && (
              <Form.File
                id='image-file'
                label='Choose File'
                custom
                onChange={uploadFileHandler}></Form.File>
            )}
          </Form.Group>
        </Col>
        <Col md={4}>
          <Form.Label>User Information</Form.Label>{" "}
          <Button
            onClick={() => setClickedInfo(!clickedInfo)}
            variant='light'
            className='btn-sm  '>
            <i className='fas fa-edit'></i>
          </Button>
          {clickedInfo ? (
            <Form onSubmit={submitHandler}>
              <Form.Group controlId='name' className='py-2'>
                <Form.Label>Name</Form.Label>
                <Form.Control
                  required
                  type='name'
                  placeholder='Enter Name'
                  value={name}
                  onChange={(e) => setName(e.target.value)}></Form.Control>
              </Form.Group>

              <Form.Group controlId='email' className='py-2'>
                <Form.Label>Email Address</Form.Label>
                <Form.Control
                  required
                  type='email'
                  placeholder='Enter Email'
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}></Form.Control>
              </Form.Group>

              <Form.Group controlId='password' className='py-2'>
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type='password'
                  placeholder='Enter Password'
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}></Form.Control>
              </Form.Group>

              <Form.Group controlId='passwordConfirm' className='py-2'>
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control
                  type='password'
                  placeholder='Confrim Password'
                  value={confirmPassword}
                  onChange={(e) =>
                    setConfirmPassword(e.target.value)
                  }></Form.Control>
              </Form.Group>
              <Button type='submit' variant='primary'>
                Update
              </Button>
            </Form>
          ) : (
            <ListGroup variant='flush'>
              <label>Name</label>
              <ListGroup.Item>{name}</ListGroup.Item>
              <label>Email</label>
              <ListGroup.Item>{email}</ListGroup.Item>
            </ListGroup>
          )}
        </Col>
        <Col md={4}>
          <Form.Label>Shipping Address</Form.Label>{" "}
          <Button
            onClick={() => setClickedShip(!clickedShip)}
            variant='light'
            className='btn-sm'>
            <i className='fas fa-edit'></i>
          </Button>
          {clickedShip ? (
            <Form onSubmit={submitHandler}>
              <Form.Group contolId='address' className='py-2'>
                <Form.Label>Address</Form.Label>
                <Form.Control
                  required
                  type='text'
                  placeholder='Enter Address'
                  value={address ? address : ""}
                  onChange={(e) => setAddress(e.target.value)}></Form.Control>
              </Form.Group>
              <Form.Group contolId='city' className='py-2'>
                <Form.Label>City</Form.Label>
                <Form.Control
                  required
                  type='text'
                  placeholder='Enter City'
                  value={city ? city : ""}
                  onChange={(e) => setCity(e.target.value)}></Form.Control>
              </Form.Group>
              <Form.Group contolId='postalCode' className='py-2'>
                <Form.Label>Postal Code</Form.Label>
                <Form.Control
                  required
                  type='text'
                  placeholder='Enter Postal Code'
                  value={postalCode ? postalCode : ""}
                  onChange={(e) =>
                    setPostalCode(e.target.value)
                  }></Form.Control>
              </Form.Group>
              <Form.Group contolId='country' className='py-2'>
                <Form.Label>Country</Form.Label>
                <Form.Control
                  required
                  type='text'
                  placeholder='Enter Country'
                  value={country ? country : ""}
                  onChange={(e) => setCountry(e.target.value)}></Form.Control>
              </Form.Group>
              <Button type='submit' variant='primary'>
                Update
              </Button>
            </Form>
          ) : (
            <ListGroup variant='flush'>
              <label>Address</label>
              <ListGroup.Item>{address}</ListGroup.Item>
              <label>City</label>
              <ListGroup.Item>{city}</ListGroup.Item>
              <label>Postal Code</label>
              <ListGroup.Item>{postalCode}</ListGroup.Item>
              <label>Country</label>
              <ListGroup.Item>{country}</ListGroup.Item>
            </ListGroup>
          )}
        </Col>
      </Row>
      <Col md={12}>
        <h2>My Orders</h2>
        {loadingOrders ? (
          <Loader />
        ) : errorOrders ? (
          <Message variant='danger'>{errorOrders}</Message>
        ) : (
          <Table responsive hover borderless striped className='table-sm'>
            <thead>
              <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Total</th>
                <th>Paid</th>
                <th>Delivered</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {orders.map((order) => (
                <tr key={order._id}>
                  <td>{order._id}</td>
                  <td>{order.createdAt.substring(0, 10)}</td>
                  <td>${order.totalPrice}</td>
                  <td>
                    {order.isPaid ? (
                      order.paidAt.substring(0, 10)
                    ) : (
                      <i className='fas fa-times' style={{ color: "red" }}></i>
                    )}
                  </td>
                  <td>
                    {order.isDelivered ? (
                      order.deliveredAt.substring(0, 10)
                    ) : (
                      <i className='fas fa-times' style={{ color: "red" }}></i>
                    )}
                  </td>
                  <td>
                    <LinkContainer to={`/order/${order._id}`}>
                      <Button className='btn-sm'>Details</Button>
                    </LinkContainer>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        )}
      </Col>
    </div>
  )
}

export default ProfileScreen
