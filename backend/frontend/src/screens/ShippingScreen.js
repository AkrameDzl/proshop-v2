import React, { useState, useEffect } from "react"
import { Form, Button, Row } from "react-bootstrap"
import { useDispatch, useSelector } from "react-redux"
import FormContainer from "../components/FormContainer"
import CheckoutSteps from "../components/CheckoutSteps"
import { saveShippingAddress } from "../actions/cartActions"
import { getUserDetails } from "../actions/userActions"

function ShippingScreen({ history }) {
  const userLogin = useSelector((state) => state.userLogin)
  const { userInfo } = userLogin
  const userDetails = useSelector((state) => state.userDetails)
  const { user } = userDetails
  const dispatch = useDispatch()
  const [address, setAddress] = useState(user.address)
  const [city, setCity] = useState(user.city)
  const [postalCode, setPostalCode] = useState(user.postalCode)
  const [country, setCountry] = useState(user.country)
  useEffect(() => {
    if (userInfo) {
      dispatch(getUserDetails("profile"))
    }
    if (!userInfo) {
      history.push("/login")
    }
  }, [history, userInfo])
  const submitHandler = (e) => {
    e.preventDefault()
    dispatch(saveShippingAddress({ address, city, postalCode, country }))
    history.push("/payment")
  }
  return (
    <FormContainer>
      <CheckoutSteps step1 step2 />
      <h1>Shipping</h1>
      <Form onSubmit={submitHandler}>
        <Form.Group controlId='address' className='py-2'>
          <Form.Label>Address</Form.Label>
          <Form.Control
            required
            type='text'
            placeholder='Enter Address'
            value={address ? address : ""}
            onChange={(e) => setAddress(e.target.value)}></Form.Control>
        </Form.Group>

        <Form.Group controlId='city' className='py-2'>
          <Form.Label>City</Form.Label>
          <Form.Control
            required
            type='text'
            placeholder='Enter City'
            value={city ? city : ""}
            onChange={(e) => setCity(e.target.value)}></Form.Control>
        </Form.Group>

        <Form.Group controlId='postalCode' className='py-2'>
          <Form.Label>Postal Code</Form.Label>
          <Form.Control
            required
            type='text'
            placeholder='Enter Postal Code'
            value={postalCode ? postalCode : ""}
            onChange={(e) => setPostalCode(e.target.value)}></Form.Control>
        </Form.Group>

        <Form.Group controlId='country' className='py-2'>
          <Form.Label>Country</Form.Label>
          <Form.Control
            required
            type='text'
            placeholder='Enter Country'
            value={country ? country : ""}
            onChange={(e) => setCountry(e.target.value)}></Form.Control>
        </Form.Group>

        <Row className='py-3'>
          <Button type='submit' variant='primary'>
            Continue
          </Button>
        </Row>
      </Form>
    </FormContainer>
  )
}

export default ShippingScreen
